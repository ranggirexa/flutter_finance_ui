// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_ui/Screens/home.dart';
import 'package:flutter_ui/Screens/statistic.dart';
import 'package:flutter_ui/data/model/add_date.dart';
import 'package:flutter_ui/widgets/bottomNavigationBar.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(AdddataAdapter());
  await Hive.openBox<Add_data>('data');
  runApp(const myApp());
}

class myApp extends StatelessWidget {
  const myApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        // statistic: Statistics(),
        home: Bottom());
  }
}
